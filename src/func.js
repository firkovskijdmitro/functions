const getSum = (str1, str2) => {
  if(typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  }
  if(!onlyNumbersOrEmpty(str1) || !onlyNumbersOrEmpty(str2) ){
    return false;
  }
  str1 = str1 !== "" ? parseInt(str1,10) : 0 ;
  str2 = str2 !== "" ? parseInt(str2,10) : 0 ;
  return (str1 + str2).toString();
};

function onlyNumbersOrEmpty(str) {
  return /^[0-9]+$/.test(str) || str === "";
}
const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var posts = 0;
  var comments = 0
  for(let item of listOfPosts){
    if(item.author === authorName){
      posts++;
    }
    if(item.hasOwnProperty('comments') === true){
      for(let comment of item.comments){
        if(comment.author === authorName){
          comments++;
        }
      }
    }
  }
  return 'Post:' + posts + ',comments:' + comments;
};

const tickets=(people)=> {
  var change = 0;
  for(let num of people){
    if(num - 25 > change){
      return 'NO';
    }
    change += 25;
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
